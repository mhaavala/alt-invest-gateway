![Alt invest](logo.png)

## Development

        $ npm install

        $ npm run start

The following configurations keys are supported as environment variables:

      COINBASE_CLIENT_ID, COINBASE_CLIENT_SECRET, COINBASE_CALLBACK_URL

Usage example:

        $ COINBASE_CALLBACK_URL=http://some-other.org npm run start

Access OAUTH redirect flows:

  * coinbase - http://localhost:3000/coinbase
  * bondora - http://localhost:3000/estateguru (Not yet implemented)
  * funderbeam - http://localhost:3000/funderbeam (Not yet implemented)

## Coinbase Flow

Redirect user to the coinbase URL and provide *callback* parameter, e.g.

      http://localhost:3000/coinbase?callback=http://test.lhv.ee/rest/coinbase?userId=12345

HINT: provide some user reference that can be used to distinguish the requestor. After user gives permissions in Coinbase page, user is redirected to the provider callback url with parameters *access_token* and *refresh_token*, e.g.

      http://test.lhv.ee/rest/coinbase?userId=12345&access_token=89e2efe79aebb5ceab0fad06b4183e124ba8a30962ca575cd59edee3705994a5&refresh_token=36d7a18ebf412a2d61206eb6879ae0908de8a5ccf54db9ba62e52e37c1dd7333

Get account information with the retrived access_token (_../coinbase/accounts?access_token={access_token}_):

        $ curl http://localhost:3000/coinbase/accounts?access_token=89e2efe79aebb5ceab0fad06b4183e124ba8a30962ca575cd59edee3705994a5

Possible error responses:

    * Success response with accounts
    * Access token revoked error

```
      {
        "errors":[
          {
            "id":"revoked_token",
            "message":"The access token  was revoked"
          }
        ]
      }    
``` 

Refresh the token (_../coinbase/refresh-token?refresh_token={refresh_token}_)
        
        $ curl http://localhost:3000/coinbase/refresh-token?refresh_token=36d7a18ebf412a2d61206eb6879ae0908de8a5ccf54db9ba62e52e37c1dd7333

Sample response:

```
        {
          "access_token":"8dd87dab7f7bc9559b6ce3ff5e2bf160681967a6a41ce8a613cdd64dacbef951",
          "token_type":"bearer",
          "expires_in":7200,
          "refresh_token":"57952f43ea19f622629f555a1af4aa37f836b700a59f58403faa0b97ee0582fb",
          "scope":"wallet:accounts:read wallet:transactions:read",
          "created_at":1536696923
        }
```

Get transactions per wallet (_../coinbase/accounts/{account_id}/transactions_), e.g.

        $ curl http://localhost:3000/coinbase/accounts/00bfb769-f7f2-5fea-bc18-63fcb735483e/transactions?access_token=8dd87dab7f7bc9559b6ce3ff5e2bf160681967a6a41ce8a613cdd64dacbef951


Get exchange rates by currency:

        $ curl http://localhost:3000/coinbase/exchange-rates?currency=EUR
 

### Coinbase links

[COINBASE CONNECT OAUTH2 REFERENCE](https://developers.coinbase.com/docs/wallet/coinbase-connect/reference)

[API](https://developers.coinbase.com/api/v2)

[Permissions](https://developers.coinbase.com/docs/wallet/permissions)

[coinbase-node](https://github.com/coinbase/coinbase-node)

[coinbase-node typescript types](https://www.npmjs.com/package/@types/coinbase)


### Bondora links
[Bondora API](https://api.bondora.com/)