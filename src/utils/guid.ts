export class GUID {
  static generate(): string {
    let get = () => {
      return Math.floor((1 + Math.random()) * 0x10000)
        .toString(16)
        .substring(1);
    };
    return get() + get() + '-' + get() + '-' + get() + '-' + get() + '-' + get() + get() + get();
  }
}