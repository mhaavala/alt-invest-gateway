import express from 'express';
import coinbase from './coinbase';
import estateguru from './estateguru';
import funderbeam from './funderbeam';

const port = process.env.PORT || 3000

let app = express();

app.use((req, res, next) => {
    res.header('Content-Type', 'application/json')
    next()
});

app.use('/coinbase', coinbase);
app.use('/estateguru', estateguru);
app.use('/funderbeam', funderbeam);

app.listen(port, (err: any) => {
	if (err) {
	  console.log(err);
	}
  
	console.log(`server is listening on ${port}`);
})