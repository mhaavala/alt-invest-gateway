import FormData from 'form-data';
import { COINBASE_CLIENT_ID, COINBASE_CLIENT_SECRET, COINBASE_CALLBACK_URL } from './settings';

export class TokenForm {

  getAuthorizationCodeForm(code: string, reference: string): FormData {
    let form = this.getBase();
    form.append('grant_type', 'authorization_code');
    form.append('code', code);
    form.append('redirect_uri', COINBASE_CALLBACK_URL + '?reference=' + reference);

    return form;
  }

  getRefreshCodeForm(token: string): FormData {
    let form = this.getBase();
    form.append('grant_type', 'refresh_token');
    form.append('refresh_token', token);

    return form;
  }
  
  private getBase() {
    let form = new FormData(); 
    form.append('client_id', COINBASE_CLIENT_ID);
    form.append('client_secret', COINBASE_CLIENT_SECRET);

    return form;
  }
}