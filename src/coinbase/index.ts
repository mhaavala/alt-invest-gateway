import express from 'express';
import { CallbackRepository } from './callback-repository';
import { 
  COINBASE_CLIENT_ID, 
  COINBASE_AUTHORIZATION_URL, 
  COINBASE_API_SCOPES, 
  COINBASE_CALLBACK_URL, 
  COINBASE_OAUTH_TOKEN_URL, 
  COINBASE_API_ACCOUNTS_URL,
  COINBASE_API_EXCHANGE_RATES_URL,
  COINBASE_API_TRANSACTIONS_URL_SUFFIX} from './settings';
import fetch from 'node-fetch';
import { TokenForm } from './token-form';

let router = express.Router();
let repository = new CallbackRepository();

let generateAuthorizationUrl = (reference: string) => {
	let callbackUrl = encodeURIComponent(COINBASE_CALLBACK_URL + '?reference=' + reference);

	return `${COINBASE_AUTHORIZATION_URL}` + 
			`client_id=${COINBASE_CLIENT_ID}` + 
      `&account=all` + 
      `&redirect_uri=${callbackUrl}` + 
			`&response_type=code` + 
			`&scope=` + encodeURIComponent(COINBASE_API_SCOPES);
 };

router.get('/', (request, response) => {
	let reference = repository.store(request.query.callback);
	response.redirect(generateAuthorizationUrl(reference));
});

router.get('/authorization-code/callback', (request, response) => {
	let callbackURl = repository.get(request.query.reference);
	let code = request.query.code;

	let form = new TokenForm().getAuthorizationCodeForm(code, request.query.reference);

	fetch(COINBASE_OAUTH_TOKEN_URL, { method: 'POST', body: form })
		.then(res => res.json())
		.then(data => {
      let includesQueryParameters = new URLSearchParams(callbackURl).toString().length > 0;
      let urlPart = includesQueryParameters ? '&' : '?';

			response.redirect(callbackURl + urlPart + 'access_token=' + data.access_token + '&refresh_token=' + data.refresh_token);
    })
    .catch(error => {
      response.status(400).end(error);
    });
});

router.get('/refresh-token', (request, response) => {
  let form = new TokenForm().getRefreshCodeForm(request.query.refresh_token);

	fetch(COINBASE_OAUTH_TOKEN_URL, { method: 'POST', body: form })
		.then(res => res.json())
		.then(data => {
			response.send(data);
    })
    .catch(error => {
      response.status(400).end(error);
    });
});

router.get('/accounts', (request, response) => {
  fetch(COINBASE_API_ACCOUNTS_URL, { headers: { 'Authorization': `Bearer ${request.query.access_token}` } })
		.then(res => res.json())
		.then(data => {
			response.send(data);
    })
    .catch(error => {
      response.status(400).end(error);
    });
});

router.get('/accounts/:account_id/transactions', (request, response) => {
  fetch(COINBASE_API_ACCOUNTS_URL + request.params.account_id + COINBASE_API_TRANSACTIONS_URL_SUFFIX, 
    { headers: { 'Authorization': `Bearer ${request.query.access_token}` } })
		.then(res => res.json())
		.then(data => {
			response.send(data);
    })
    .catch(error => {
      response.status(400).end(error);
    });
});

router.get('/exchange-rates', (request, response) => {
  fetch(COINBASE_API_EXCHANGE_RATES_URL + `?currency=${request.query.currency}`, {})
    .then(res => res.json())
    .then(data => {
      response.send(data);
    })
    .catch(error => {
      response.status(400).end(error);
    });
})

export default router;