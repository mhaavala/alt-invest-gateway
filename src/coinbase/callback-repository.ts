import { GUID } from "../utils/guid";

// TODO - replace with real database to storing/retrieval 

export class CallbackRepository {
  callbacks = new Map<string, string>();

  store(callback: string): string {
    let key = this.generateId();
    this.callbacks.set(key, callback);

    return key;
  }

  get(key: string): string {
    return this.callbacks.get(key);
  }

  private generateId() {
    return GUID.generate();
  }
  
}