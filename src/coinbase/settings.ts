export const COINBASE_CLIENT_ID = process.env.COINBASE_CLIENT_ID || 'b3bd05ab40665efc3352aa7e2a26c058dca182443839f3be814c7cb8686f591d';
export const COINBASE_CLIENT_SECRET = process.env.COINBASE_CLIENT_SECRET || '74ee5000175a88d22f6e4451b562334c213bfca58ebe6d7e7810e63a96fc9d40';
export const COINBASE_AUTHORIZATION_URL = 'https://www.coinbase.com/oauth/authorize?';
export const COINBASE_OAUTH_TOKEN_URL = 'https://api.coinbase.com/oauth/token';
export const COINBASE_API_BASE_URL = 'https://api.coinbase.com/v2/';
export const COINBASE_API_ACCOUNTS_URL = COINBASE_API_BASE_URL + 'accounts/';
export const COINBASE_API_EXCHANGE_RATES_URL = COINBASE_API_BASE_URL + 'exchange-rates';
export const COINBASE_API_TRANSACTIONS_URL_SUFFIX = '/transactions';


export const COINBASE_API_SCOPES = 'wallet:accounts:read,wallet:transactions:read';
export const COINBASE_CALLBACK_URL = process.env.COINBASE_CALLBACK_URL || 'http://localhost:3000/coinbase/authorization-code/callback';