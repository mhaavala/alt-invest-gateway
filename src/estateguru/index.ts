import express from 'express';
import { NOT_YET_IMPLEMENTED } from '../utils/responses';

let router = express.Router();

router.get('/', (req, res) => {
    res.send(NOT_YET_IMPLEMENTED);
})

export default router;